import geoip2.database
import pyshark
import socket
import sys
import getopt
from datetime import datetime

my_ip = socket.gethostbyname(socket.gethostname())


def get_ip_location(ip, language="de"):

    with geoip2.database.Reader("./GeoLite2-City.mmdb") as reader:
        # Get location data based on ip
        rs = reader.city(ip)

        # Get needed data from location data object
        try:
            country = rs.country.names[language]
            subdivision = rs.subdivisions.most_specific.names[language]
            city = rs.city.names[language]
            postalCode = rs.postal.code
            return country, subdivision, city, postalCode
        except:
            raise Exception("Error")

    return "Error", "Error", "Error", "Error"


def getOmeIp(localIp,):
    displayfilter = "stun && ip.src == " + localIp + " && frame.len == 106"
    capture = pyshark.LiveCapture(
        interface=r'\Device\NPF_{95349B38-E448-4D78-87A7-5C0DEA578760}',  display_filter=displayfilter)

    oldIp = "0"

    for packet in capture.sniff_continuously():
        ip = packet["ip"].dst
        if oldIp != ip:
            now = datetime.now()
            oldIp = ip
            print(now.strftime("%H:%M:%S"), " -> IP-Adress: ", ip)
            try:
                country, subdivision, city, postalCode = get_ip_location(ip)
                print(">>", city, postalCode, subdivision, country)
            except:
                try:
                    country, subdivision, city, postalCode = get_ip_location(
                        ip, language="en")
                    print(">>", city, postalCode, subdivision, country)
                except:
                    print("Error")


localIp = "192.168.0.25"
getOmeIp(localIp)
